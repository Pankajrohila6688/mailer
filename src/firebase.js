import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCezaQPLU50_w0lK4oO-nBrs_7rAjjNiVg",
    authDomain: "clone-facd8.firebaseapp.com",
    projectId: "clone-facd8",
    storageBucket: "clone-facd8.appspot.com",
    messagingSenderId: "1094288069058",
    appId: "1:1094288069058:web:37452ab20dde7eaf49065b",
    measurementId: "G-B78T09SGVQ"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { db, auth, provider };